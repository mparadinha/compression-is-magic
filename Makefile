all:
	gcc smol_unzip.c -o smol_unzip -O2 -Wno-unused-result

.PHONY: clean

clean:
	rm smol_unzip
