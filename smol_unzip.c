#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#define massert(cond, msg) if (!(cond)) { fprintf(stderr, msg); exit(1); }

// the End of Central Directory (EOCDR struct), which is at the very end of the file,
// tells us how to find the central directory which is basically a bunch of FileHeader(s) 
// in a row.
// Each FileHeader points us to a LocalFileHeader. The actual data is after this struct. 

typedef struct {
    uint8_t sig[4]; // 'P', 'K', 5, 6
    uint16_t disk_nbr;
    uint16_t cd_start_disk;
    uint16_t disk_cd_entries;
    uint16_t cd_entries;
    uint32_t cd_size;
    uint32_t cd_offset;
    uint16_t comment_len;

    uint8_t* comment;
} __attribute__((__packed__)) EOCDR; // end of central directory

typedef struct {
    uint8_t sig[4]; // 'P', 'K', 1, 2
    uint16_t version_made_by;
    uint16_t version_needed;
    uint16_t flags;
    uint16_t compression_method;
    uint16_t last_mod_time;
    uint16_t last_mod_date;
    uint32_t crc;
    uint32_t compressed_size;
    uint32_t uncompressed_size;
    uint16_t filename_len;
    uint16_t extra_field_len;
    uint16_t file_comment_len;
    uint16_t disk_nbr_start;
    uint16_t internal_file_attrs;
    uint32_t external_file_attrs;
    uint32_t rel_offset; // (of local header) relative to start of disk where file is

    uint8_t* filename;
    uint8_t* extra_field;
    uint8_t* file_comment;
} __attribute__((__packed__)) FileHeader;

typedef struct {
    uint8_t sig[4]; // 'P', 'K', 3, 4
    uint16_t version_needed; 
    uint16_t flags;
    uint16_t compression_method;
    uint16_t last_mod_time;
    uint16_t last_mod_date;
    uint32_t crc;
    uint32_t compressed_size;
    uint32_t uncompressed_size;
    uint16_t filename_len;
    uint16_t extra_field_len;

    uint8_t* filename;
    uint8_t* extra_field;
} __attribute__((__packed__)) LocalFileHeader;

bool check_sig(uint8_t contents[], const uint8_t sig[4]) {
    for (size_t i = 0; i < 4; i++) {
        if (contents[i] != sig[i]) return false;
    }
    return true;
}

EOCDR find_eocdr(FILE* file) {
    // the End of Central Directory (EOCDR) struct is always at the very end of the zip file.
    // because it can have an optional comment (of length up to 2^16 - 1) we don't know
    // exactly where it starts, so we need to search for it. (it has a signature, i.e. it
    // starts with the bytes 'P' 'K' 5 6)

    // load the part of the file where EOCDR can start
    // it's not very cache money to load whole file if its like 10GB

    // comment can have u16 length
    size_t max_eocdr_size = (sizeof(EOCDR) - sizeof(uint8_t*)) + (1 << 16);

    fseek(file, 0, SEEK_END);
    size_t file_size = ftell(file);
    if (max_eocdr_size < file_size) fseek(file, -max_eocdr_size, SEEK_END);
    else fseek(file, 0, SEEK_SET);

    uint8_t* search_space = malloc(max_eocdr_size);
    size_t bytes_read = fread(search_space, 1, max_eocdr_size, file);

    const uint8_t eocdr_sig[4] = {'P', 'K', 5, 6};
    for (size_t i = 0; i < bytes_read; i++) {
        if (check_sig(search_space + i, eocdr_sig)) {
            EOCDR ret;
            memcpy(&ret, search_space + i, sizeof(EOCDR));

            if (ret.comment_len != 0) {
                ret.comment = malloc(ret.comment_len);
                memcpy(ret.comment, search_space + i + (sizeof(EOCDR) - sizeof(uint8_t*)), ret.comment_len);
            }
            else {
                ret.comment = NULL;
            }

            free(search_space);

            return ret;
        }
    }

    fprintf(stderr, "Could not find 'End of Central Directory'. Might not be a zip file\n");
    exit(EXIT_FAILURE);
}

FileHeader read_fileheader(FILE* zip) {
    FileHeader ret = {0};
    fread(&ret, sizeof(FileHeader) - 3 * sizeof(uint8_t*), 1, zip);
    if (ret.filename_len != 0) {
        ret.filename = malloc(ret.filename_len + 1);
        fread(ret.filename, 1, ret.filename_len, zip);
        ret.filename[ret.filename_len] = 0;
    }
    if (ret.extra_field_len != 0) {
        ret.extra_field = malloc(ret.extra_field_len + 1);
        fread(ret.extra_field, 1, ret.extra_field_len, zip);
        ret.extra_field[ret.extra_field_len] = 0;
    }
    if (ret.file_comment_len != 0) {
        ret.file_comment = malloc(ret.file_comment_len + 1);
        fread(ret.file_comment, 1, ret.file_comment_len, zip);
        ret.file_comment[ret.file_comment_len] = 0;
    }
    return ret;
}

LocalFileHeader read_localheader(FILE* zip) {
    LocalFileHeader ret = {0};
    fread(&ret, sizeof(LocalFileHeader) - 2 * sizeof(uint8_t*), 1, zip);
    if (ret.filename_len != 0) {
        ret.filename = malloc(ret.filename_len + 1);
        fread(ret.filename, 1, ret.filename_len, zip);
        ret.filename[ret.filename_len] = 0;
    }
    if (ret.extra_field_len != 0) {
        ret.extra_field = malloc(ret.extra_field_len + 1);
        fread(ret.extra_field, 1, ret.extra_field_len, zip);
        ret.extra_field[ret.extra_field_len] = 0;
    }
    return ret;
}

typedef struct {
    uint8_t* data;
    size_t data_len;
    size_t byte_pos;
    uint8_t bit_pos;
} BitStream;

uint64_t consume(BitStream* s, uint8_t n_bits) {
    massert(n_bits <= 64, "trying to read more than 64bits at once from BitStream\n");
    massert(n_bits <= 8 * (s->data_len - s->byte_pos) - s->bit_pos, "end of BitStream\n");

    uint64_t ret = 0;
    uint8_t bits_left = n_bits;

    // align to next full byte if possible
    if (bits_left + s->bit_pos >= 8) {
        uint8_t n = (8 - s->bit_pos);

        ret |= (s->data[s->byte_pos] >> s->bit_pos) & (0xff >> s->bit_pos);

        s->bit_pos = 0;
        s->byte_pos++;

        bits_left -= n;
    }

    // read whole bytes
    while (bits_left > 8) {
        ret |= s->data[s->byte_pos] << (n_bits - bits_left);
        s->byte_pos++;
        bits_left -= 8;
    }

    // read any remaning bits
    if (bits_left != 0) {
        uint8_t left = (s->data[s->byte_pos] >> s->bit_pos) & (0xff >> (8 - bits_left));
        ret |= (left << (n_bits - bits_left));
        s->bit_pos += bits_left;
        bits_left = 0;
    }

    return ret;
}

#define HUFFMAN_MAX_BITLEN 15 // deflate has max of 15 bits per codeword

typedef struct {
    // build the binary tree.
    // left child of node at idx is 2 * idx + 1
    // right child of node at idx is 2 * idx + 2
    // value of -1 means node has children (and not data), -2 means not initialized
    // if it is a leaf node then the value if the symbol you're looking for

    // we need to have 16 levels to the tree because of the 15 bit maximum for the bit lengths 
    int16_t tree[1 << 16]; // 2^16 spots
} HuffmanDecoder;

// bit lengths for the fixed huffman codes (for litlen and dist)
uint8_t fixed_litlen_bitlens[288] = {
    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, // 0 - 143
    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, // 144 - 255
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, // 256 - 279
    8, 8, 8, 8, 8, 8, 8, 8, // 280 - 287
};
uint8_t fixed_dist_bitlens[32] = {
    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, // 0 - 31
};
HuffmanDecoder fixed_litlen_dec, fixed_dist_dec;

void huffman_init_decoder(HuffmanDecoder* dec, uint8_t* bitlens, size_t bitlens_n) {
    for (size_t i = 0; i < 1 << 16; i++) { dec->tree[i] = -2; }
    dec->tree[0] = -1;

    size_t lvl_idx = 1; // index where current level begins
    size_t first_free = 0; // first free node at depth i - 1
    for (uint32_t i = 1; i <= HUFFMAN_MAX_BITLEN; i++) {
        size_t found = 0;
        size_t this_lvl_start = first_free * 2;
        for (size_t j = 0; j < bitlens_n; j++) {
            uint8_t bitlen = bitlens[j];
            if (bitlen != i) continue;

            dec->tree[lvl_idx + this_lvl_start + found] = j;

            found++;
        }

        first_free = first_free * 2 + found; // last used node on this level

        // the rest of the nodes on this level are gonna be parent nodes for next level
        for (size_t n = first_free; n < 1 << i; n++) {
                dec->tree[lvl_idx + n] = -1;
        }

        lvl_idx = lvl_idx * 2 + 1;
    }

}

// passing decoder with pointer because its huge (64k), not because we modify it
uint16_t huffman_decode_symbol(HuffmanDecoder* dec, BitStream* s) {
    uint16_t bits = 0;
    size_t tree_idx = 0;
    uint8_t bits_read = 0;
    while (bits_read < HUFFMAN_MAX_BITLEN) {
        bool bit = consume(s, 1);
        bits += bit;
        bits <<= 1;
        bits_read++;
        if (bit) { // right 
            tree_idx = tree_idx * 2 + 2;
        }
        else { // left
            tree_idx = tree_idx * 2 + 1;
        }

        int16_t sym = dec->tree[tree_idx];
        if (sym != -1 && sym != -2) {
            return (uint16_t)sym;
        }
    }

    massert(false, "could not decode symbol\n");
    return 0xffff;
}

void expand_huffman_codes(HuffmanDecoder* litlen_dec, HuffmanDecoder* dist_dec, BitStream* stream) {
    // instead of sending the huffman tables for symbol -> code translation
    // only the bit lengths of each code are sent, as this is enough info to
    // decode the symbols. (why would one do this? probably because it makes
    // compressing the huffman table info easier; lots of repeating numbers)
    //
    // the literals and back reference lengths (lit_len) of the LZ77 are encoded using
    // huffman codes. the distances of these back references are also encoded with
    // huffman codes (different codes though).
    //
    // if 'dynamic' these bit lenghts (of the symbols from the huffman tree) are
    // run length encoded (RLE).
    // to decode the RLE there is another huffman code (called codelen below)

    uint16_t litlen_codes_n = consume(stream, 5) + 257;
    uint8_t dist_codes_n = consume(stream, 5) + 1;
    uint8_t codelen_codes_n = consume(stream, 4) + 4; // 4bits=0-15 + 4 = 19 

    // the bit lengths for the codelen huffman code are sent in this particular
    // order. if we get less then 19 of these then the rest are set to 0
    static const uint8_t codelen_bitlens_order[] = {
       16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15
    };

    uint8_t codelen_bitlens[19] = {0};
    for (uint32_t i = 0; i < codelen_codes_n; i++) {
        codelen_bitlens[codelen_bitlens_order[i]] = consume(stream, 3);
    }

    HuffmanDecoder codelen_dec;
    huffman_init_decoder(&codelen_dec, codelen_bitlens, 19);

    // now we can use this huffman tree (codelen) to decode the, run length encoded,
    // bit lengths for the litlen and dist huffman tree, which follow in the stream.
    // the meaning of the codelen symbols for the RLE is as follows:
    // 0-15 represents a bit length 0-15
    // 16 represents "copy prev bit length 3-6 times"
    // 17 represents "repeat bit length of 0 3-10 times"
    // 18 represents "repeat bit length of 0 11-138 times"

    uint8_t* litlen_dist_bitlens = malloc(litlen_codes_n + dist_codes_n);
    uint16_t last_len = 0;
    size_t i = 0;
    while (i < litlen_codes_n + dist_codes_n) {
        uint16_t sym = huffman_decode_symbol(&codelen_dec, stream);

        if (sym < 16) { // bit len of 0 to 15
            litlen_dist_bitlens[i++] = sym;
            last_len = sym;
        }
        else if (sym == 16) { // copy prev bitlen 3 to 6 times (read 2 extra bits)
            uint8_t extra = consume(stream, 2);
            for (size_t n = 0; n < 3 + extra; n++) { 
                litlen_dist_bitlens[i++] = last_len;
            }
        }
        else if (sym == 17) { // repeat bitlen of 0, 3 to 10 times (read 3 extra bits) 
            uint8_t extra = consume(stream, 3);
            for (size_t n = 0; n < 3 + extra; n++) { 
                litlen_dist_bitlens[i++] = 0;
                last_len = 0;
            }
        }
        else if (sym == 18) { // repeat bitlen of 0, 11 to 138 times (read 7 extra bits) 
            uint8_t extra = consume(stream, 7);
            for (size_t n = 0; n < 11 + extra; n++) { 
                litlen_dist_bitlens[i++] = 0;
                last_len = 0;
            }
        }
    }

    // once we have the huffman trees for the litlen/dist codes we can use those trees
    // to decode symbols from the stream. and use a map from those symbols to LZ77 info
    // (i.e. literals, back references, dists, etc)

    uint8_t* litlen_bitlens = litlen_dist_bitlens;
    uint8_t*   dist_bitlens = litlen_dist_bitlens + litlen_codes_n;

    huffman_init_decoder(litlen_dec, litlen_bitlens, litlen_codes_n);
    huffman_init_decoder(dist_dec, dist_bitlens, dist_codes_n);
}

size_t inflate_block(HuffmanDecoder* litlen_dec, HuffmanDecoder* dist_dec, BitStream* stream, uint8_t* uncomp_data) {
    uint16_t backref_size_extra_bits[] = {
        0, 0, 0, 0, 0, 0, 0, 0, // symbols 257 through 264 (inclusive)
        1, 1, 1, 1,             // 265-268
        2, 2, 2, 2,             // 269-272
        3, 3, 3, 3,             // 273-276
        4, 4, 4, 4,             // 277-280
        5, 5, 5, 5,             // 281-284
        0, 0, 0                 // 285-287
    };
    uint16_t backref_size_base[] = {
        3, 4, 5, 6, 7, 8, 9, 10,
        11, 13, 15, 17,
        19, 23, 27, 31,
        35, 43, 51, 59,
        67, 83, 99, 115,
        131, 163, 195, 227,
        258, 0, 0
    };

    uint16_t backref_dist_extra_bits[] = {
        0, 0, 0, 0, 1, 1, 2, 2,   
        3, 3, 4, 4, 5, 5, 6, 6,
        7, 7, 8, 8, 9, 9, 10, 10,
        11, 11, 12, 12, 13, 13,
        0, 0
    };
    uint16_t backref_dist_base[] = {
        1, 2, 3, 4, 5, 7, 9, 13,
        17, 25, 33, 49, 65, 97, 129, 193,
        257, 385, 513, 769, 1025, 1537, 2049, 3073,
        4097, 6145, 8193, 12289, 16385, 24577,
        0, 0
    };

    size_t bytes_done = 0;

    while (true) {
        uint16_t sym = huffman_decode_symbol(litlen_dec, stream);

        if (sym == 256) { break; } // EOF

        if (sym < 256) { // normal literal character
            uncomp_data[bytes_done++] = sym;
            continue;
        }

        // symbol that we read must be some back reference

        uint8_t extra_len = consume(stream, backref_size_extra_bits[sym - 257]);
        uint16_t backref_size = backref_size_base[sym - 257] + extra_len;

        sym = huffman_decode_symbol(dist_dec, stream);
        uint16_t extra_dist = consume(stream, backref_dist_extra_bits[sym]);
        uint16_t backref_copy_dist = backref_dist_base[sym] + extra_dist;

        for (uint16_t n = 0; n < backref_size; n++) {
            uncomp_data[bytes_done] = uncomp_data[bytes_done - backref_copy_dist];
            bytes_done++;
        }
    }

    return bytes_done;
}

void decompress(uint8_t* comp_data, uint8_t* uncomp_data, size_t comp_data_n) {
    BitStream stream = {
        .data = comp_data, .data_len = comp_data_n,
        .byte_pos = 0, .bit_pos = 0
    };

    size_t bytes_done = 0;
    bool last_block;
    do {
        last_block = consume(&stream, 1);
        uint8_t block_type = consume(&stream, 2);

        size_t bytes_written = 0;
        switch (block_type) {
            case 0: { // non compressed block
                if (stream.bit_pos != 0) consume(&stream, 8 - stream.bit_pos); // read until next byte
                uint16_t block_size = 0;
                memcpy(&block_size, stream.data + stream.byte_pos, sizeof(block_size));
                stream.byte_pos += 4; // ignore the two's complement of block_size that comes after

                memcpy(uncomp_data, comp_data + stream.byte_pos, block_size);
                stream.byte_pos += block_size;
                bytes_written = block_size;

                break;
            }
            case 1: { // fixed huffman block
                // decoder were already initialized for this case because they're fixed
                bytes_written = inflate_block(&fixed_litlen_dec, &fixed_dist_dec, &stream, uncomp_data);

                break;
            }
            case 2: { // dynamic huffman block
                HuffmanDecoder litlen_dec, dist_dec;
                expand_huffman_codes(&litlen_dec, &dist_dec, &stream);

                bytes_written = inflate_block(&litlen_dec, &dist_dec, &stream, uncomp_data);

                break;
            }

            case 3: massert(false, "invalid block type, error in compressed data\n")
            default: massert(false, "invalid block type\n")
        }

        bytes_done += bytes_written;
        uncomp_data += bytes_written;

    } while(!last_block);
}

void extract(char* filename) {
    FILE* zipfile = fopen(filename, "rb");
    if (!zipfile) { printf("Could not open '%s'\n", filename); return; }

    EOCDR eocdr = find_eocdr(zipfile);

    FileHeader central_dir[eocdr.cd_entries];
    fseek(zipfile, eocdr.cd_offset, SEEK_SET);
    for (size_t i = 0; i < eocdr.cd_entries; i++) {
        FileHeader fheader = read_fileheader(zipfile); 
        central_dir[i] = fheader;

        massert((fheader.version_made_by & 0xff00) == 0x0300, "file is not for UNIX. aborting\n")
        massert(!(fheader.flags & 0x1), "file is encrypted. aborting\n")
        massert(!(fheader.flags & 0x8), "uses data descriptor, not supported. aborting\n")
        if (!(fheader.external_file_attrs & 0x40000000)) { // if not directory 
            massert(fheader.compression_method == 8, "file does not use DEFLATE. aborting.\n")
        }
    }

    // the fixed huffman decoder are the same for all files so might as well
    // init them now (only one once)
    huffman_init_decoder(&fixed_litlen_dec, fixed_litlen_bitlens, 288);
    huffman_init_decoder(&fixed_dist_dec, fixed_dist_bitlens, 32);

    for (size_t i = 0; i < eocdr.cd_entries; i++) {
        fseek(zipfile, central_dir[i].rel_offset, SEEK_SET);
        LocalFileHeader lheader = read_localheader(zipfile);

        if (central_dir[i].external_file_attrs & 0x40000000) { // if its a directory
            int mkdir_ret = mkdir(lheader.filename, S_IRWXO | S_IRWXG | S_IRWXU);
            if (mkdir_ret != 0) {
                struct stat sb;
                massert(stat(lheader.filename, &sb) == 0, "could not create directory\n")
            }
            continue;
        }

        uint8_t* comp_data = malloc(lheader.compressed_size);
        fread(comp_data, 1, lheader.compressed_size, zipfile);

        uint8_t* data = malloc(lheader.uncompressed_size);
        decompress(comp_data, data, lheader.compressed_size);

        FILE* uncomp_file = fopen(lheader.filename, "wb");
        massert(uncomp_file, "could not create file\n");

        fwrite(data, 1, lheader.uncompressed_size, uncomp_file);
        fclose(uncomp_file);
    }

    fclose(zipfile);
}

void list_files(char* filename) {
    FILE* zipfile = fopen(filename, "rb");
    if (!zipfile) { printf("Could not open '%s'\n", filename); return; }

    EOCDR eocdr = find_eocdr(zipfile);

    fseek(zipfile, eocdr.cd_offset, SEEK_SET);
    printf("files found inside '%s':\n", filename);
    for (size_t i = 0; i < eocdr.cd_entries; i++) {
        FileHeader fheader = read_fileheader(zipfile); 
        printf("\t%s\n", fheader.filename);
    }

    fclose(zipfile);
}

void print_usage(char* progname) {
    printf("%s: [cmd] [zipfile] [files]\n", progname);
    printf("options for [cmd]:\n");
    printf("    x - extract [zipfile]\n");
    printf("    l - list files in [zipfile]\n");
    //printf("    z - DEFLATE [files] into a new [zipfile]\n");
}

int main(int argc, char** argv) {
    if (argc < 2) {
        print_usage(argv[0]);
        return 0;
    }

    if (strcmp(argv[1], "x") == 0) {
        if (argc < 3) { print_usage(argv[0]); return 0; }
        extract(argv[2]);
    }
    else if (strcmp(argv[1], "l") == 0) {
        if (argc < 3) { print_usage(argv[0]); return 0; }
        list_files(argv[2]);
    }
    //else if (strcmp(argv[1], "z") == 0) {
    //    massert(false, "woopsie, not implemented the DEFLATE yet\n");
    //}
    else {
        print_usage(argv[0]);
    }

    return 0;
}
